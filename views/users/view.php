<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = $model->name;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'login',
            'email:email',
            'name:ntext',
            'address:ntext',
        ],
    ])
    ?>
    <?php if ($model->photo) : ?>
        <img src='uploads/<?php echo $model->photo ?>'>
    <?php endif; ?>

    <p>
    <table cellpadding="5" border="1">
        <tr>
            <td>
                <h4>Реферальная ссылка: </h4> 
            </td>
            <td>
                http://localhost/test/web/index.php?r=users/create&p=<?php echo $model->hash ?>
            </td>
        <tr>
    </table>
</p>
<p>
    <?php
    $referals = Users::findReferals($model->id);
    if ($referals) :
        ?>
    <table cellpadding="5" border="1">
        <tr>
            <td>
                <h4>Ваши рефералы:</h4> 
            </td>
        </tr>
        <?php foreach ($referals as $referal): ?>
            <tr>
                <td>
                    <?= $referal->login ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
</p>

<p>
    <a href="index.php?r=site/top">Топ юзеров по количеству рефералов</a>
</p>

</div>
