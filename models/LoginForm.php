<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;

class LoginForm extends Model {

    public $login;
    public $password;
    private $_user = false;

    public function rules() {
        return [
            [['login', 'password'], 'required'],
            ['password', 'validatePassword'],
        ];
    }

    public function login() {
        if ($this->validate()) {
            $user = Users::findByUsername($this->login);
            Yii::$app->user->login($user, 3600 * 24 * 30);
            return true;
        } else {
            return false;
        }
    }

    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function getUser() {
        if ($this->_user === false) {
            $this->_user = Users::findByUsername($this->login);
        }

        return $this->_user;
    }

}
