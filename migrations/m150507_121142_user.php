<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_121142_user extends Migration {

    public function up() {
        $this->createTable('users', [
            'id' => 'pk',
            'login' => Schema::TYPE_STRING . ' NOT NULL UNIQUE',
            'email' => Schema::TYPE_STRING . ' NOT NULL UNIQUE',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'name' => Schema::TYPE_TEXT,
            'address' => Schema::TYPE_TEXT,
            'photo' => Schema::TYPE_TEXT,
            'hash' => Schema::TYPE_TEXT,
            'referal_id' => Schema::TYPE_INTEGER,
            'auth_key' => Schema::TYPE_TEXT,
            'access_token' => Schema::TYPE_TEXT 
        ]);
    }

    public function down() {
        $this->dropTable('users');
    }

}
