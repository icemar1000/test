<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Users extends ActiveRecord implements \yii\web\IdentityInterface {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['login', 'email', 'password'], 'required'],
            [['name', 'address', 'photo', 'hash'], 'string'],
            [['referal_id'], 'integer'],
            [['login', 'email', 'password', 'access_token', 'auth_key'], 'string', 'max' => 255],
            [['login', 'email'], 'unique'],
            [['photo'], 'file', 'extensions' => 'jpg, jpeg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'email' => 'E-mail',
            'password' => 'Password',
            'name' => 'Name',
            'address' => 'Address',
            'photo' => 'Photo',
            'hash' => 'Hash',
            'referal_id' => 'Referal ID',
            'access_token' => 'access Token',
            'auth_key' => 'auth Key'
        ];
    }

    public function resizeImage() {
        $image = 'uploads/' . $this->photo;
        $height = Yii::$app->params['image_height'];
        $width = Yii::$app->params['image_width'];
        list($w_i, $h_i, $type) = getimagesize($image);
        if ($w_i > $width || $h_i > $height) {
            $types = array("", "gif", "jpeg", "png");
            $ext = $types[$type];
            if ($ext) {
                $func = 'imagecreatefrom' . $ext;
                $img_i = $func($image);
            } else {
                return false;
            }

            $w_o = $width;
            $h_o = $width / ($w_i / $h_i);

            if ($h_o > $height) {
                $w_o = $height / ($h_i / $w_i);
                $h_o = $height;
            }

            $img_o = imagecreatetruecolor($w_o, $h_o);
            imagecopyresampled($img_o, $img_i, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
            $func = 'image' . $ext;

            return $func($img_o, $image);
        } else {
            return TRUE;
        }
    }

    public static function findByUsername($login) {
        $user = Users::findOne(['login' => $login]);

        if ($user) {
            return new static($user);
        }

        return null;
    }

    public static function findByHash($hash) {
        $user = Users::findOne(['hash' => $hash]);

        if ($user && count(Users::findReferals($user->id)) < Yii::$app->params['max_referal']) {
            return new static($user);
        }

        return null;
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findReferals($id) {
        $users = Users::find()->where(array('referal_id' => $id))->all();
        return $users;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        foreach (self::$users as $user) {
            if ($user['access_token'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey() {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey) {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $password;
    }

    public static function createTop() {
        $users = Users::find()->all();
        foreach ($users as $user) {
            $top[$user->login] = count(Users::findReferals($user->id));
        }
        return $top;
    }

}
