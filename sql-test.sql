#1 - максимальная разница температур для каждой станции
#за день
select
  w1.where_id,
  date_format(w1.date,'%Y-%m-%d') as `day`,
  max(abs(w1.temperature - w2.temperature)) as `max_diff`
from weather as w1 
join weather as w2 on 
  w1.where_id = w2.where_id and 
  date_format(w1.date,'%Y-%m-%d') = date_format(w2.date,'%Y-%m-%d') and
  w1.id != w2.id
group by 
  where_id;

#за месяц  
select
  w1.where_id,
  date_format(w1.date,'%Y-%m') as `month`,
  max(abs(w1.temperature - w2.temperature)) as `max_diff`
from weather as w1 
join weather as w2 on 
  w1.where_id = w2.where_id and 
  date_format(w1.date,'%Y-%m') = date_format(w2.date,'%Y-%m') and
  w1.id != w2.id
group by 
  where_id;

 #за неделю 
select
  w1.where_id,
  date_format(w1.date,'%u') as `week`,
  max(abs(w1.temperature - w2.temperature)) as `max_diff`
from weather as w1 
join weather as w2 on 
  w1.where_id = w2.where_id and 
  date_format(w1.date,'%u') = date_format(w2.date,'%u') and
  w1.id != w2.id
group by 
  where_id;

#2 - разница температур больше 10
#за день
select
  date_format(w1.date, '%Y-%m-%d') as `day`,
  abs(w1.temperature - w2.temperature) as `diff`
from weather as w1 
join weather as w2 on
  date_format(w1.date, '%Y-%m-%d') = date_format(w2.date, '%Y-%m-%d') and
  w1.id != w2.id
where (w1.temperature - w2.temperature) > 10
group by
  date_format(w1.date, '%Y-%m-%d');

#за месяц  
select
  date_format(w1.date, '%Y-%m') as `month`,
  abs(w1.temperature - w2.temperature) as `diff`
from weather as w1 
join weather as w2 on
  date_format(w1.date, '%Y-%m') = date_format(w2.date, '%Y-%m') and
  w1.id != w2.id
where (w1.temperature - w2.temperature) > 10
group by
  date_format(w1.date, '%Y-%m');

#за неделю  
select
  date_format(w1.date, '%u') as `week`,
  abs(w1.temperature - w2.temperature) as `diff`
from weather as w1 
join weather as w2 on
  date_format(w1.date, '%u') = date_format(w2.date, '%u') and
  w1.id != w2.id
where (w1.temperature - w2.temperature) > 10
group by
  date_format(w1.date, '%u');

select (
(select count(*) from (select count(*) from weather where precipitation = 'rain_snow' 
#*тут любые дополнительные условия, т.к. из задания не понятно, как определить гололед  
group by date_format(date, '%Y-%m-%d')) as sleet_days)
/
(select count(*) from (select count(*) from weather group by date_format(date, '%Y-%m-%d')) as days)
) as sleet_count,
  date_format(date, '%Y') as `year`
from weather
group by
  date_format(date, '%Y');

  
#4 - оптимизированный вариант таблицы
CREATE TABLE `weather` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`where_id` INT(11) NOT NULL DEFAULT '0',
	`temperature` SMALLINT(4) NOT NULL DEFAULT '0',
	`precipitation` ENUM('none','rain','rain_snow','snowfall','hail') NOT NULL DEFAULT 'none',
	`wind` SMALLINT(4) UNSIGNED NOT NULL DEFAULT '0',
	`date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

